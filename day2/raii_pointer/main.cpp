#include <iostream>

using namespace std;

class Gadget
{
    int id_;
public:
    Gadget(int id) : id_(id) {
        cout << "ctor " << id_ << endl;
    }
    Gadget(const Gadget& a)
    {
        id_ = a.id_;
        cout << "copy ctor " << id_ << endl;
    }

    Gadget(Gadget&& other) noexcept
    {
        id_ = other.id_;
        cout << "move ctor" << endl;
    }

    void print()
    {
        cout << "print " << id_ << endl;
    }
    ~Gadget()
    {
        cout << "dtor " << id_ << endl;
    }
};

template<class T>
class raii_ptr
{
    T* ptr_{nullptr};
public:
    raii_ptr(T* ptr) : ptr_(ptr) {}

    // non-copyable
    raii_ptr(const raii_ptr&) = delete;
    raii_ptr operator=(const raii_ptr&) = delete;

    // move semantic
    raii_ptr(raii_ptr&& rop)
    {
        cout << "mv ctor" << endl;
        swap(ptr_,rop.ptr_);
    }

    ~raii_ptr()
    {
        delete ptr_;
    }

    T& operator*() const
    {
        return *ptr_;
    }

    T* operator->() const
    {
        return ptr_;
    }
};

class Obj
{
    raii_ptr<Gadget> id_;
public:
    Obj() : id_(new Gadget(10)) {};
};

raii_ptr<Gadget> generate(int n)
{
    return raii_ptr<Gadget>(new Gadget{n});
}

int main()
{
    cout << "Hello RAII pointer!" << endl;
    raii_ptr<Gadget> p(new Gadget(1));
    p->print();
    (*p).print();
    raii_ptr<Gadget> p2 = generate(2);
    p2->print();
    raii_ptr<Gadget> p3(move(p2));
    p3->print();
    Obj o;
    Obj b(move(o));
    return 0;
}

