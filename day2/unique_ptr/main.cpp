#include <iostream>
#include <memory>
#include <vector>
#include <string>

using namespace std;

class Gadget
{
    int id_;
    string s_;
public:
    Gadget(int id) : id_(id) {
        cout << "ctor " << id_ << endl;
    }

    Gadget(int id, const string& s) : id_(id), s_(s) {
        cout << "ctor " << id_ << endl;
    }

    Gadget(const Gadget& a)
    {
        id_ = a.id_;
        cout << "copy ctor " << id_ << endl;
    }

    Gadget(Gadget&& other) noexcept
    {
        id_ = other.id_;
        cout << "move ctor" << endl;
    }

    virtual void print()
    {
        cout << "print " << id_ << endl;
    }
    virtual ~Gadget()
    {
        cout << "dtor " << id_ << endl;
    }
};

//template<typename T, typename... A>
//unique_ptr<T> make_unique(A&&... a)
//{
//    return unique_ptr<T>(new T(std::forward<A>(a)...));
//}

unique_ptr<Gadget> make_gadget(int n)
{
    return unique_ptr<Gadget>(new Gadget(n));
}

void sink(unique_ptr<Gadget> val)
{
    val->print();
}

void process(const unique_ptr<Gadget>& p)
{
    p->print();
}

class SuperGadget : public Gadget
{
public:
    SuperGadget() : Gadget(10)
    {
    }

    void print()
    {
        cout << "Super gadget" << endl;
    }

    virtual ~SuperGadget()
    {
        cout << "Super gadget dtor" << endl;
    }
};

int main()
{
    cout << "Hello unique_ptr!" << endl;
//    unique_ptr<Gadget> p(new Gadget(1));
//    auto p2 = make_unique<Gadget>(2, "ala");
//    sink(make_gadget(3));
//    process(p2);
//    sink(move(p2));
//    process(make_unique<Gadget>(10));
    vector<unique_ptr<Gadget>> v;
    v.push_back(make_unique<Gadget>(10));
    v.emplace_back(new SuperGadget());
    v.push_back(make_unique<SuperGadget>());
    for (auto& el : v)
        el->print();

    unique_ptr<Gadget> ptr(make_unique<Gadget>(100));
    sink(move(ptr));

    //auto bounded_task = bind(&sink, move(ptr)); // does not work
    //auto bounded_task = bind([](unique_ptr<Gadget>& v) { sink(move(v)); }, move(ptr));
    //auto bounded_task = [ptr=move(ptr)]() mutable { sink(move(ptr)); };

    bounded_task();


    return 0;
}

