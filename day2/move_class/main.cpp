#include <iostream>
#include <vector>

using namespace std;

class Array
{
    int* arr_ = nullptr;
    size_t size_{0};

public:
    Array(const Array&) = delete;
    Array& operator=(const Array&) = delete;

    //Array(Array&& other) = default; // does not work
    //Array& operator=(Array&&) = default;

    Array(size_t size, int val) : size_(size)
    {
        cout << "ctor" << endl;
        if (size_ > 0)
        {
            arr_ = new int[size_];
            for (size_t i = 0 ; i < size_ ; ++i)
                arr_[i] = val;
        }
    }

    Array(Array&& other) // noexcept(noexcept(swap<int, int>))
    {
        swap(arr_, other.arr_);
        swap(size_,other.size_);
    }

    void print()
    {
        cout << "[ ";
        for (size_t i = 0 ; i < size_ ; ++i)
            cout << arr_[i] << ", ";
        cout << "]" << endl;
    }

    ~Array()
    {
        delete[] arr_;
    }
};

Array gen_array()
{
    return Array(5, -3);
}

int main()
{
    cout << "Hello World!" << endl;    
    Array a(10, -1);
    a.print();
    Array b(1, -2);
    //b = a;
    b.print();
    Array c{gen_array()}; // works, because of optimisations
    c.print();
    //Array d(a);
    vector<Array> v;
    v.push_back(gen_array());
    return 0;
}

