#include <iostream>
#include <vector>

using namespace std;

class Gadget
{
    int id_;
public:
    Gadget(int id) : id_(id) {
        cout << "ctor " << id_ << endl;
    }
    Gadget(const Gadget& a)
    {
        id_ = a.id_;
        cout << "copy ctor " << id_ << endl;
    }

    Gadget(Gadget&& other) noexcept
    {
        id_ = other.id_;
        cout << "move ctor" << endl;
    }

    void initialize()
    {
        cout << "init " << id_ << endl;
    }
    ~Gadget()
    {
        cout << "dtor " << id_ << endl;
    }
};

Gadget gen(int i)
{
    Gadget g(i);
    g.initialize();
    return g;
}

int main()
{
    cout << "Hello World!" << endl;
    //Gadget window = gen(0);
    cout << "-- Vector ------------" << endl;
    vector<Gadget> v;
    //v.reserve(10);
    v.push_back(gen(1));
    cout << "-- insert 2 --- " << endl;
    v.push_back(gen(2));
    cout << "-- end--------" << endl;
    return 0;
}

