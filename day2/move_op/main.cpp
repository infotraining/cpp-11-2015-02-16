#include <iostream>
#include <string>

using namespace std;

//void foo(string s)
//{
//    cout << "foo(string s)" << endl;
//}

void foo(const string& s)
{
    cout << "foo(const string& s)" << endl;
}

//void foo(string& s)
//{
//    cout << "foo(string& s)" << endl;
//}

void foo(string s)
{
    cout << "foo(string&& s)" << endl;
}

int main()
{
    cout << "Hello World!" << endl;
    foo(string("ala"));
    string b("ala");
    foo(b);
    foo(move(b));
    cout << b << endl; // we should not do that
    return 0;
}

