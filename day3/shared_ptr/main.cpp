#include <iostream>
#include <memory>
#include <string>

using namespace std;

/*class Human // bad
{
    shared_ptr<Human> partner_;
    string name_;
public:
    Human(string name) : name_(name)
    {
        cout << "ctor of " << name_ << endl;
    }

    ~Human()
    {
        cout << "dtor of " << name_ << endl;
    }

    void set_partner(shared_ptr<Human> partner)
    {
        partner_ = partner;
    }

    void description() const
    {
        cout << "My name is " << name_;
        if (partner_)
            cout << " and my partner is " << partner_->name_;
        cout << endl;
    }

};*/

class Human // good
{
    weak_ptr<Human> partner_;
    string name_;
public:
    Human(string name) : name_(name)
    {
        cout << "ctor of " << name_ << endl;
    }

    ~Human()
    {
        cout << "dtor of " << name_ << endl;
    }

    void set_partner(weak_ptr<Human> partner)
    {
        partner_ = partner;
    }

    void description() const
    {
        cout << "My name is " << name_;
        auto sp_partner = partner_.lock();
        if (sp_partner)
            cout << " and my partner is " << sp_partner->name_;
        cout << endl;
    }

};

int main()
{
    cout << "Hello World!" << endl;
    auto h1 = make_shared<Human>("adam");
    auto h2 = make_shared<Human>("ewa");
    h1->set_partner(h2);
    h2->set_partner(h1);
    h1->description();
    h2->description();
    return 0;
}

