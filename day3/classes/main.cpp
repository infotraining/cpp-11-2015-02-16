#include <iostream>
#include <memory>

using namespace std;

class Base {
public:
    virtual void doWork() final
    {
        cout << "do work from Base" << endl;
    }

    virtual void mf1(int i) const
    {
        cout << "Base::mf1" << endl;
    }

    virtual void mf2(const int& i)
    {
        cout << "Base::mf2" << endl;
    }
};

class Derived : public Base
{
public:
//    void doWork()
//    {
//        cout << "Derived::dowork" << endl;
//    }

    virtual void mf1(int i) const override
    {
        cout << "Derived::mf1" << endl;
    }

    virtual void mf2(const int& i) override
    {
        cout << "Derived::mf2" << endl;
    }
};


int main()
{
    cout << "Hello World!" << endl;
    unique_ptr<Base> pb = make_unique<Derived>();
    pb->mf1(1);
    int i{};
    pb->mf2(i);
    return 0;
}

