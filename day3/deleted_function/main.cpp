#include <iostream>

using namespace std;

bool is_odd(int n)
{
    return n % 2;
}

bool is_odd(char) = delete;
bool is_odd(double) = delete;

int main()
{
    cout << "Hello World!" << endl;
    cout << "is 3 odd " << is_odd(3) << endl;
    cout << "is a odd " << is_odd('a') << endl;
    cout << "is 3.14 odd " << is_odd(3.14) << endl;
    return 0;
}

