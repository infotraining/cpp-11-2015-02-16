#include <iostream>
#include <vector>
#include <algorithm>
#include <functional>
#include <boost/type_index.hpp>
#include <memory>

using namespace std;
using namespace std::placeholders;

void f()
{
    cout << "f()" << endl;
}

struct Lessthan
{
    int op;
    Lessthan(int op) : op(op) {}
    bool operator()(int n) { return n < op; }
};

void do_something(void(*f)())
{
    f();
}

void do_something_in_a_modern_way(std::function<void()> f)
{
    f();
}

//auto make_adder(int n) // C++14 only
auto make_adder(int n) -> function<int(int)>
{
    return [n](int a){ return a+n; };
}

struct Gadget
{
    void do_stuff()
    {
        cout << "stuff is done" << endl;
    }
};

auto make_lambda_from_unique_ptr(unique_ptr<Gadget> sink_param)
{
    return [sink_param = move(sink_param)]() { sink_param->do_stuff(); };
}

auto main() -> int
{
    cout << "Hello lambdas!" << endl;
    void(*g)() = f;
    g();

    int a{};

    auto g1 = + [](){ cout << "lambda" << endl;};
    //auto g2 = + [a](){ cout << "lambda for a" << a << endl;}; // not working with capture
    auto g2 = [a](){ cout << "lambda for a" << a << endl;};
    auto g3 = [a](){ cout << "lambda for b" << a << endl;};

    cout << boost::typeindex::type_id_with_cvr<decltype(g1)>().pretty_name() << endl;
    cout << boost::typeindex::type_id_with_cvr<decltype(g2)>().pretty_name() << endl;
    cout << boost::typeindex::type_id_with_cvr<decltype(g3)>().pretty_name() << endl;

    std::function<void()> g4 = [a](){ cout << "lambda for a " << a << endl;}; // less optimal solution

    g(); g2();  g3();

//    auto add3 = make_adder(3);
//    auto add5 = make_adder(5);

    int n = 3;
    auto add3 = [n](int a) -> double
    {
        return a+n;
    };


    auto add3_by_bind = bind([](int a, int b){ return a+b; }, 3, _1);
    n = 5;
    auto add5 = [n](int a){ return a+n; };

    cout << "add3 + 10 = " <<  add3_by_bind(10) << endl;
    cout << "add5 + 10 = " <<  add5(10) << endl;

    do_something([](){ cout << "lambda in function" << endl;});
    //do_something([a](){ cout << "lambda for a" << a << endl;}); // not working
    do_something_in_a_modern_way([a](){ cout << "lambda for a " << a << endl;});



    vector<int> v{0, 1,2,3,4};
    vector<int> less_than;
    vector<int> greater_than;

    //    auto bi = back_inserter(less_than);
    //    bi = 1;
    //    bi++;
    //    bi = 2;
    //    for (auto& el : less_than)
    //        cout << el << ", ";
    //    cout << endl;

    int op = 1;
    partition_copy(v.begin(), v.end(),
                   back_inserter(less_than),
                   back_inserter(greater_than),
                   //Lessthan(3));
                   [&op](int n) { return op > n; });

    cout << "less than 2 = ";
    for (auto& el : less_than)
        cout << el << ", ";
    cout << endl;

    cout << "greater than 2 = ";
    for (auto& el : greater_than)
        cout << el << ", ";
    cout << endl;

    // mutable

    auto l = [greater_than]() mutable {
        for (auto& el : greater_than)
            el += 2;
        return greater_than;
    };

    auto v_res = l();
    cout << "vres = ";
    for (auto& el : v_res)
        cout << el << ", ";
    cout << endl;

    // C++14 lambdas
    cout << "C++14 lambdas ......... " << endl;
    auto add = [](auto a, auto b) { return a+b; };
    cout << "add int " << add(3,4) << endl;

    cout << "add string " << add("ala"s,"ola"s) << endl;

    auto uni_ptr = make_unique<int>(5);
    auto play = [uni_ptr = move(uni_ptr)]() { cout << *uni_ptr << endl; };
    play();
    play();

    auto unique_lambda = make_lambda_from_unique_ptr(make_unique<Gadget>());
    unique_lambda();
    return 0;
}

