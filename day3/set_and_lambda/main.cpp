#include <iostream>
#include <set>
#include <functional>

using namespace std;

struct Gadget
{
    int id;
    Gadget(int id) : id(id) {}
    void do_stuff() const
    {
        cout << "stuff is done in " << id << endl;
    }
};

int main()
{
    cout << "Hello World!" << endl;

    auto comp = [](const Gadget& a, const Gadget& b) { return a.id < b.id ;};
    set<Gadget, decltype(comp)>  s(comp);

    /*set<Gadget,std::function<bool(const Gadget&, const Gadget&)>>
            s([](const Gadget& a, const Gadget& b) { return a.id > b.id ;});*/

    s.insert(Gadget(1));
    s.insert(Gadget(2));

    for(auto& el : s)
        el.do_stuff();

    return 0;
}

