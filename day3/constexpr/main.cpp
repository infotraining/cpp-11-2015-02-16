#include <iostream>
#include <array>

using namespace std;

constexpr int cube(int n)
{
    return n*n*n;
}

constexpr size_t factorial(size_t n)
{
    return (n == 0) ? 1 : n * factorial(n-1);
}

int main()
{
    array<int, factorial(cube(2))> arr{1,2,3}; // too long to compile for 3
    int n;
    cin >> n;
    cout << factorial(cube(3)) << endl;
    return 0;
}

