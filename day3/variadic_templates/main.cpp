#include <iostream>
#include <memory>

using namespace std;

// counter

template<typename... A>
struct Count;

template<typename First, typename... Rest>
struct Count<First, Rest...>
{
    constexpr static int value = 1 + Count<Rest...>::value;
};

template<>
struct Count<>
{
    constexpr static int value = 0;
};

// adder

template<typename T>
T adder(T v)
{
    return v;
}

template<typename T, typename... Rest>
T adder(T first, Rest... rest)
{
    return first + adder(rest...);
}


template<typename T, typename... A>
unique_ptr<T> my_make_unique(A&&... a)
{
    return unique_ptr<T>(new T(std::forward<A>(a)...));
}

int main()
{
    cout << "Hello World!" << endl;
    cout << Count<int, int, double>::value << endl;
    cout << adder(1,2,3,4,5) << endl;
    cout << adder(10,20) << endl;
    return 0;
}

