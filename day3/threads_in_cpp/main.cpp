#include <iostream>
#include <future>
#include <thread>

using namespace std;

int f()
{
    this_thread::sleep_for(1s);
    return 42;
}

int g(int val)
{
    this_thread::sleep_for(1s);
    if (val == 13) throw std::logic_error("13 is unlucky");
    return val;
}

future<int> getmefuture()
{
    return async(launch::async, f);
}

int main()
{
    cout << "Hello World!" << endl;
    future<int> res1 = async(launch::async, f);
    future<int> res2 = async(launch::async, g, 13);

    auto res3 = getmefuture();

    cout << "After call" << endl;
    cout << "result = " << res1.get() << endl;
    res2.wait();
    this_thread::sleep_for(5s);
    cout << "result 2 is ready - checking it" << endl;
    try
    {
        cout << "result = " << res2.get() << endl;
    }
    catch (const std::logic_error& err)
    {
        cerr << "got error " << err.what() << endl;
    }
    cout << "result3 = " << res3.get() << endl;
}


