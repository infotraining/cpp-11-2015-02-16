#include <iostream>

using namespace std;

enum class Coffee : unsigned char {espresso, cappucino, latte};

int main()
{
    cout << "Hello enums!" << endl;
    Coffee my_coffe = Coffee::cappucino;
    int a = static_cast<int>(Coffee::latte);

    auto c = Coffee::latte;
    switch (c) {
    case Coffee::latte:
        cout << "latte" << endl;
        break;
    default:
        cout << "default coffee" << endl;
        break;
    }

    return 0;
}

