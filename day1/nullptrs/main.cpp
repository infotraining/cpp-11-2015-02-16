#include <iostream>

using namespace std;

void f_ptr(int *ptr)
{
    if(ptr != 0)
    {
        cout << "f(int* " << ptr << ")" << endl;
    }
    else
    {
        cout << "f(NULL)" << endl;
    }
}

void f_ptr(int v)
{
    cout << "f(int = " << v << endl;
}

int main()
{
    cout << "Hello World!" << endl;
    int a = 100;
    f_ptr(&a);
    f_ptr(static_cast<int*>(NULL)); //does not work - ambigious
    f_ptr(nullptr);
    return 0;
}

