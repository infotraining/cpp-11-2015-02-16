#include <iostream>
#include <vector>

using namespace std;

struct Gadget
{
    int id;
    Gadget(int id) : id(id) {}
};


template<class Cont>
auto find_null(const Cont& c) -> decltype(begin(c))
{
    auto it = begin(c);
    for( ; it != end(c) ; ++it)
    {
        if (!*it) break;
    }
    return it;
}

void f(Gadget * ptr)
{
    //do_something(prt->it);
}

int main()
{
    cout << "Find null!" << endl;
    //vector<Gadget*> v = {new Gadget{1}, new Gadget{2}, nullptr, new Gadget{3}};
    Gadget* v[] = {new Gadget{1}, new Gadget{2}, nullptr, new Gadget{3}};
//    for (auto el : v)
//    {
//        cout << el->id << ", " << endl;
//    }

    f(v[1]);

    for (auto it = begin(v) ; it != find_null(v) ; ++it)
    {
        cout << (*it)->id << ", " << endl;
    }
    return 0;
}

