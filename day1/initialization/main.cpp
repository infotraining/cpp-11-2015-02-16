#include <iostream>
#include <vector>
#include <chrono>
#include <array>
#include <boost/type_index.hpp>

using namespace std;

template<typename Cont>
void print(const Cont& c)
{
    for(auto& el : c)
        cout << el << ", ";
    cout << endl;
}

struct Point
{
    int x{};
    int y{};
    Point(int x, int y) :x(x), y(y) // C++98
    {
        cout << "ctor with parameters called" << endl;
    }

    Point(initializer_list<int> l)
    {
        cout << "ctor with initalizer list" << endl;
        print(l);
    }
};

int main()
{
    cout << "Initialization" << endl;

    auto a = int{1};
    vector<int> v{1,2,3,4,5};
    array<int, 4> arr{1,2,3,4};
    print(arr);
    print(v);
    Point p{10, 20};
    Point p1(10, 20);
    cout << "Point : " << p.x << ", " << p.y << endl;

    vector<int> test_ctor1{10,20};
    print(test_ctor1);
    vector<int> test_ctor2(10,20);
    print(test_ctor2);

    // since C++14

    auto raw_string = R"(ala ma kota)";
    auto s = "ala ma kota"s; // std::string
    cout << boost::typeindex::type_id_with_cvr<decltype(s)>().pretty_name() << endl;

    auto t = 10s; // std::chrono
    cout << boost::typeindex::type_id_with_cvr<decltype(t)>().pretty_name() << endl;
    auto t2 = 3min; // std::chrono
    cout << boost::typeindex::type_id_with_cvr<decltype(t2)>().pretty_name() << endl;

    return 0;
}

