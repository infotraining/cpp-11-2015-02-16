#include <iostream>
#include <typeinfo>
#include <boost/type_index.hpp>
#include <vector>

using namespace std;

double res()
{
    return 3.1415;
}

template<typename Cont>
void print(const Cont& c)
{
    int s = c.size(); // narrowing to int...
    auto s2 = c.size(); // not narrowing size

}

int main()
{
    cout << "Hello World!" << endl;

    int a = 0; // soo 98

    auto b = 0.0; // C++11 - forces initialization
    auto c = int{0};

    int r_int = res(); // C++98
    auto r = static_cast<int>(res()); // C++11

    auto r2 = res();

    cout << "Result = " << r << endl;

    const int& ref_a = a;
    auto a_ref_a = ref_a; // really it is a copy of a
    const auto& true_a_ref = ref_a;
    cout << "Type of a_ref_a: ";
    cout << boost::typeindex::type_id_with_cvr<decltype(a_ref_a)>().pretty_name() << endl;
    cout << " " << typeid(a_ref_a).name() << endl;

    cout << "Type of true_ref_a: ";
    cout << boost::typeindex::type_id_with_cvr<decltype(true_a_ref)>().pretty_name() << endl;
    cout << " " << typeid(true_a_ref).name() << endl;

    int tab[3] = {1,2,3};

    for(auto& el : tab)   // syntactic sugar
        cout << el << ", ";
    cout << endl;

    for(auto it = begin(tab) ; it != end(tab) ; ++it)
    {
        cout << *it << ", ";
    }
    cout << endl;

    vector<string> v{"3","4","5"};
    //vector<string> v2{"6","7","8"};

    for(auto it = begin(v) ; it != end(v) ; ++it)
    {
        cout << *it << ", ";
    }
    cout << endl;

//    for (auto&& el : v)
//        cout << el << ", ";
//    cout << endl;

    for (const auto& el : v) // important because auto guesses just "string", nor ref to string
        cout << el << ", ";
    cout << endl;

    vector<bool> bv{0,1,0,1};

    cout << bv[1] << endl;

    for (auto&& el : bv) // rvalue, because el is temporary object
    {
        cout << el << ", ";
    }

    auto bvelement = bv[2]; // bvelement is not a bool (_bit_reference)
    auto bvelement_bool = static_cast<bool>(bv[2]); // cast to bool

    cout << endl;

    //volatile discussion

    /*volatile int a;
    for (int i = 0 ; i < 100 ; ++i)
    {
        // operation on a;
        process(a);
        sleep(100ms);
    }*/

    //f(shared_ptr<Gadget>(new Gadget), my_fun_may_throw()); // bad...
    //f(make_shared(Gadget_params), my_fun() ); // good

    //int t = 0;
    //cout << t++ << t++ << t++ << endl;

    return 0;
}

