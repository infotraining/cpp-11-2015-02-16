#include <iostream>

using namespace std;

template <typename A, typename B>
auto add(A a, B b) -> decltype(a+b) // C++11 i C++14
{
    return a + b;
}

template <typename A, typename B>
decltype(auto) add2(A a, B b) // C++14
{
    return a + b;
}

template <typename A, typename B>
auto add3(A a, B b) // C++14, but loosess const, volatile and ref on exit
{
    return a + b;
}


auto main() -> int
{
    cout << "Decltype" << endl;
    auto a = 1;
    decltype(a + 10.0) b;
    b = 12.3;
    cout << b << endl;

    //
    cout << "add = " << add2(a, b) << endl;
    return 0;
}

